demoart-visualmusic.pdf:

%.pdf: %.tex
	pdflatex $<
	- bibtex $*
	pdflatex $<
	pdflatex $<

clean:
	$(RM)  *.log *.aux \
	*.cfg *.glo *.idx *.toc \
	*.ilg *.ind *.out *.lof \
	*.lot *.bbl *.blg *.gls *.cut *.hd \
	*.dvi *.ps *.thm *.tgz *.zip *.rpi \
	*.pdf *.4ct *.4tc *.css *.html *.idv \
	*.lg *.tmp *.xref

open: demoart-visualmusic.pdf
	xdg-open $<

edit:
	gvim -p *.tex *.bib Makefile
	zotero &

watch:
	watch make
