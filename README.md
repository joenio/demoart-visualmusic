# demoart-visualmusic

Paper about demo art and demoscene culture, and its relation to visual music,
submitted to #18.ART https://18art.medialab.ufg.br/brasil

Presented on october 04 2019, slides:
* http://joenio.me/demoart-visualmusic

Published preprint:
* https://doi.org/10.31219/osf.io/48wfp

## Compiling

   sudo apt install texlive-bibtex-extra
   make

## LICENSE

GNU GPL v3

## AUTHOR

Joenio Marques da Costa
